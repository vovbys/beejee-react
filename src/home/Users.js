/* eslint-disable no-script-url */

import React, {useState} from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Title from './Title';
import UserService from "../services/UserService";


export default function Users() {
    const [userService] = useState(new UserService());
    const [rows, setRows] = useState([]);
    if (rows.length === 0) {
        userService.retrieverUsers().then(rows => setRows(rows));
    }


    return (
        <React.Fragment>
            <Title>All users</Title>
            <Table size="medium">
                <TableHead>
                    <TableRow>
                        <TableCell>UUID</TableCell>
                        <TableCell>Name</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {rows.map(row => (
                        <TableRow key={row.userId}>
                            <TableCell>{row.userId}</TableCell>
                            <TableCell>{row.name}</TableCell>
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
        </React.Fragment>
    );
}
