import React from 'react';
import MaterialTable from 'material-table';
import UserService from "../services/UserService";
import TaskService from "../services/TaskService";

export default function Tasks() {
    const [state, setState] = React.useState({
        columns: [
            {title: 'taskId', field: 'taskId', editable: 'never'},
            {title: 'description', field: 'description'},
            {title: 'mail', field: 'mail'},
            {
                title: 'userId',
                field: 'userId',
                lookup: null,
            },
        ],
        data: null,
        userService: new UserService(),
        taskService: new TaskService()
    });

    if (state.data === null && state.columns[3].lookup !== null) {
        state.taskService.retrieverTasks()
            .then(data => setState({...state, data}));
    }

    if (state.columns[3].lookup === null) {
        state.userService.retrieverUsers()
            .then(rows => {
                    let lookup = {};
                    rows.forEach(element => lookup["" + element.userId + ""] = element.name);
                    state.columns[3].lookup = lookup;
                    setState({...state});
                }
            );
    }

    return (
        <MaterialTable
            title="Tasks"
            columns={state.columns}
            data={state.data === null ? [] : state.data}
            editable={{
                onRowAdd: newData =>
                    new Promise(resolve => {
                        resolve();
                        state.taskService.add(newData)
                            .then(() => {
                                    const data = null;
                                    setState({...state, data});
                                }
                            )


                    })
                ,
                onRowUpdate: (newData, oldData) =>
                    new Promise(resolve => {
                        resolve();
                        state.taskService.update(newData)
                            .then(() => {
                                const data = null;
                                setState({...state, data});
                            })
                    }),
                onRowDelete:
                    oldData =>
                        new Promise(resolve => {
                            resolve();
                            state.taskService.delete(oldData)
                                .then(() => {
                                    const data = null;
                                    setState({...state, data});
                                })
                        }),
            }
            }
        />
    );

}
