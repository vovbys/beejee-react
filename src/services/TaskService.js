function logout() {
    localStorage.clear();
    window.location.href = '/index.html';
}

async function save(item) {
    fetch("/tasks/save",
        {
            method: "POST",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(item)
        })
        .then(response => {
            if (!response.ok) {
                this.handleResponseError(response);
            }
        })
        .catch(error => {
            this.handleError(error);
            //logout();
        });
}

class TaskService {
    items;

    createData(taskId, description, mail, userId) {
        return {taskId, description, mail, userId}
    }

    constructor() {
        this.items = [
            this.createData(2000, "task for admin", "admin@ivbys.com", "36c17ed0-f67c-4722-8ccf-0a968cc6e445"),
            this.createData(2001, "task for user", "user@ivbys.com", "caf2e8ab-09c4-4599-82e0-c408d95291fd")
        ];
    }

    async retrieverTasks() {
        return fetch("/views/tasks",
            {
                method: "GET",
                mode: "no-cors",
                headers: {
                    "Content-Type": "application/json",
                }
            })
            .then(response => {
                if (!response.ok) {
                    this.handleResponseError(response);
                }
                return response.json();
            })
            .then(json => {
                console.log("Retrieved items:");
                console.log(json);
                return json;
            })
            .catch(error => {
                this.handleError(error);
                logout();
            });
    }

    async add(item) {
        return save(item);
    }

    async update(item) {
        return save(item);
    }

    async delete(item) {
        return fetch("/tasks/delete/" + item.taskId,
            {
                method: "DELETE",
                headers: {
                    "Content-Type": "application/json",
                }
            })
            .then(response => {
                if (!response.ok) {
                    this.handleResponseError(response);
                }
            })
            .catch(error => {
                this.handleError(error);
                logout();
            });
    }

    handleResponseError(response) {
        throw new Error("HTTP error, status = " + response.status);
    }

    handleError(error) {
        console.log(error.message);
    }


}

export default TaskService;