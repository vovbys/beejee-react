import Configuration from "../Configuration/Configuration";

class UserService {

    constructor() {
        this.config = new Configuration();
    }



    async retrieverUsers() {
        return fetch("/views/users",
            {
                method: "GET",
                mode:"no-cors",
                headers: {
                    "Content-Type": "application/json",
                }
            })
            .then(response => {
                if (!response.ok) {
                    this.handleResponseError(response);
                }
                return response.json();
            })
            .then(json => {
                console.log("Retrieved items:");
                console.log(json);
                return json;
            })
            .catch(error => {
                this.handleError(error);
                localStorage.clear();
                window.location.href = '/index.html';
            });
    }




    handleResponseError(response) {
        throw new Error("HTTP error, status = " + response.status);
    }

    handleError(error) {
        console.log(error.message);
    }
}

export default UserService;