import React from 'react';
import './App.css';
import SignIn from "./login/SignIn";
import Home from "./home/Home";

const params = new URLSearchParams(window.location.search);


function App() {
    if (params.get('page') === "home") {
        return (
            <Home/>
        );
    }
    return (
        <SignIn error={params.get('error')}/>
    );
}

export default App;
